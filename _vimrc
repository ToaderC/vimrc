" 11.08.2019


source $VIMRUNTIME/vimrc_example.vim

set guifont=consolas:h12
colorscheme vsdark
highlight Normal guibg=grey7


" Relative numbers to the cursor position
set relativenumber

" Display cursor position
set ruler

" Use the OS clipboard
set clipboard=unnamed

" Indent length to 4
set tabstop=4
set shiftwidth=4
" Indent inserts spaces instead of TAB
set expandtab

" Case insensitive if search is lowercase
set ignorecase

" Case sensitive if search is not all lowercase
set smartcase

" Save backup, swap and undo files to %userprofile%/vimtmp
" // ensure no file name conflict
" ,. uses current directory if ~/vimtmp is not found
set backupdir=~/vimtmp//,.
set directory=~/vimtmp//,.
set undodir=~/vimtmp//,.

" Alt+j - Add a blank line below the current line and move cursor down.
nnoremap <silent><A-j> :set paste<CR>o<Esc>:set nopaste<CR>

" Shift-ALT+j - Add a blank line below the current.
nnoremap <silent><S-A-j> :set paste<CR>m`o<Esc>``:set nopaste<CR>

" Alt-k Add a blank line above the current line and move cursor up.
nnoremap <silent><A-k> :set paste<CR>O<Esc>:set nopaste<CR>

" Shift-Alt-k Add a blank line above the current line.
nnoremap <silent><S-A-k> :set paste<CR>m`O<Esc>``:set nopaste<CR>

" Disable arrow keys for navigation
nnoremap <Left> :echo "Use h"<CR>
nnoremap <Right> :echo "Use l"<CR>
nnoremap <Up> :echo "Use k"<CR>
nnoremap <Down> :echo "Use j"<CR>

" Scroll slightly faster
nnoremap <C-e> 2<C-e>
nnoremap <C-y> 2<C-y>

" d will delete and not cut
nnoremap d "_d

" Generated vimrc code
set diffexpr=MyDiff()
function MyDiff()
  let opt = '-a --binary '
  if &diffopt =~ 'icase' | let opt = opt . '-i ' | endif
  if &diffopt =~ 'iwhite' | let opt = opt . '-b ' | endif
  let arg1 = v:fname_in
  if arg1 =~ ' ' | let arg1 = '"' . arg1 . '"' | endif
  let arg1 = substitute(arg1, '!', '\!', 'g')
  let arg2 = v:fname_new
  if arg2 =~ ' ' | let arg2 = '"' . arg2 . '"' | endif
  let arg2 = substitute(arg2, '!', '\!', 'g')
  let arg3 = v:fname_out
  if arg3 =~ ' ' | let arg3 = '"' . arg3 . '"' | endif
  let arg3 = substitute(arg3, '!', '\!', 'g')
  if $VIMRUNTIME =~ ' '
    if &sh =~ '\<cmd'
      if empty(&shellxquote)
        let l:shxq_sav = ''
        set shellxquote&
      endif
      let cmd = '"' . $VIMRUNTIME . '\diff"'
    else
      let cmd = substitute($VIMRUNTIME, ' ', '" ', '') . '\diff"'
    endif
  else
    let cmd = $VIMRUNTIME . '\diff'
  endif
  let cmd = substitute(cmd, '!', '\!', 'g')
  silent execute '!' . cmd . ' ' . opt . arg1 . ' ' . arg2 . ' > ' . arg3
  if exists('l:shxq_sav')
    let &shellxquote=l:shxq_sav
  endif
endfunction
