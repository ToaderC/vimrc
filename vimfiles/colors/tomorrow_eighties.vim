" Name:         Tomorrow-Night-Eighties
" Author:       Chris Kempson <http://chriskempson.com>
" Maintainer:   Gianmaria Bajo <mg1979.git@gmail.com>
" License:      Vim License (see `:help license`)
" Last Updated: ven 11 ott 2019 09:07:12 CEST

" Generated by Colortemplate v2.0.0

set background=dark

hi clear
if exists('syntax_on')
  syntax reset
endif

let g:colors_name = 'tomorrow_eighties'

let s:t_Co = exists('&t_Co') && !empty(&t_Co) && &t_Co > 1 ? &t_Co : 2
let s:italics = (((&t_ZH != '' && &t_ZH != '[7m') || has('gui_running')) && !has('iOS'))

if (has('termguicolors') && &termguicolors) || has('gui_running')
  hi Normal guifg=#cccccc guibg=#262626 guisp=NONE gui=NONE cterm=NONE
  call vsd#init('tomorrow_eighties')
  hi EndOfBuffer guifg=#393939 guibg=NONE guisp=NONE gui=NONE cterm=NONE
  hi LineNr guifg=#393939 guibg=NONE guisp=NONE gui=NONE cterm=NONE
  hi NonText guifg=#393939 guibg=NONE guisp=NONE gui=NONE cterm=NONE
  hi Search guifg=#262626 guibg=#f2c38f guisp=NONE gui=NONE cterm=NONE
  hi IncSearch guifg=#262626 guibg=#f99157 guisp=NONE gui=bold cterm=bold
  hi TabLine guifg=#393939 guibg=fg guisp=NONE gui=reverse cterm=reverse
  hi TabLineFill guifg=#393939 guibg=fg guisp=NONE gui=reverse cterm=reverse
  hi StatusLine guifg=#393939 guibg=#ffcc66 guisp=NONE gui=reverse cterm=reverse
  hi StatusLineNC guifg=#393939 guibg=fg guisp=NONE gui=reverse cterm=reverse
  hi StatusLineTerm guifg=#393939 guibg=#ffcc66 guisp=NONE gui=reverse cterm=reverse
  hi StatusLineTermNC guifg=#393939 guibg=fg guisp=NONE gui=reverse cterm=reverse
  hi VertSplit guifg=#262626 guibg=#393939 guisp=NONE gui=NONE cterm=NONE
  hi Visual guifg=NONE guibg=#264f78 guisp=NONE gui=NONE cterm=NONE
  hi VisualNOS guifg=NONE guibg=#264f78 guisp=NONE gui=NONE cterm=NONE
  hi Directory guifg=#6699cc guibg=NONE guisp=NONE gui=NONE cterm=NONE
  hi ModeMsg guifg=#99cc99 guibg=NONE guisp=NONE gui=NONE cterm=NONE
  hi MoreMsg guifg=#99cc99 guibg=NONE guisp=NONE gui=NONE cterm=NONE
  hi Question guifg=#99cc99 guibg=NONE guisp=NONE gui=NONE cterm=NONE
  hi WarningMsg guifg=#f2777a guibg=NONE guisp=NONE gui=NONE cterm=NONE
  hi MatchParen guifg=#f2777a guibg=#393939 guisp=NONE gui=bold cterm=bold
  hi Folded guifg=#999999 guibg=NONE guisp=NONE gui=NONE cterm=NONE
  hi FoldColumn guifg=fg guibg=NONE guisp=NONE gui=NONE cterm=NONE
  hi CursorLine guifg=NONE guibg=#393939 guisp=NONE gui=NONE cterm=NONE
  hi CursorLineNr guifg=#ffcc66 guibg=NONE guisp=NONE gui=NONE cterm=NONE
  hi CursorColumn guifg=fg guibg=#393939 guisp=NONE gui=NONE cterm=NONE
  hi SignColumn guifg=fg guibg=NONE guisp=NONE gui=NONE cterm=NONE
  hi ColorColumn guifg=fg guibg=#393939 guisp=NONE gui=NONE cterm=NONE
  hi Comment guifg=#999999 guibg=NONE guisp=NONE gui=NONE cterm=NONE
  hi Todo guifg=#999999 guibg=NONE guisp=NONE gui=NONE cterm=NONE
  hi Title guifg=#999999 guibg=NONE guisp=NONE gui=NONE cterm=NONE
  hi Constant guifg=#f99157 guibg=NONE guisp=NONE gui=NONE cterm=NONE
  hi String guifg=#99cc99 guibg=NONE guisp=NONE gui=NONE cterm=NONE
  hi Identifier guifg=fg guibg=NONE guisp=NONE gui=italic cterm=italic
  hi Function guifg=#61afef guibg=NONE guisp=NONE gui=NONE cterm=NONE
  hi Statement guifg=#f2777a guibg=NONE guisp=NONE gui=NONE cterm=NONE
  hi Conditional guifg=#6699cc guibg=NONE guisp=NONE gui=NONE cterm=NONE
  hi Repeat guifg=#6699cc guibg=NONE guisp=NONE gui=NONE cterm=NONE
  hi Label guifg=#6699cc guibg=NONE guisp=NONE gui=NONE cterm=NONE
  hi Operator guifg=#66cccc guibg=NONE guisp=NONE gui=NONE cterm=NONE
  hi Type guifg=#ffcc66 guibg=NONE guisp=NONE gui=NONE cterm=NONE
  hi Structure guifg=#ffcc66 guibg=NONE guisp=NONE gui=NONE cterm=NONE
  hi Special guifg=#f2c38f guibg=NONE guisp=NONE gui=NONE cterm=NONE
  hi SpecialKey guifg=#f2c38f guibg=NONE guisp=NONE gui=NONE cterm=NONE
  hi PreProc guifg=#cc99cc guibg=NONE guisp=NONE gui=NONE cterm=NONE
  hi Define guifg=#cc99cc guibg=NONE guisp=NONE gui=NONE cterm=NONE
  hi Include guifg=#cc99cc guibg=NONE guisp=NONE gui=NONE cterm=NONE
  hi SpellBad guifg=#f2777a guibg=NONE guisp=NONE gui=undercurl cterm=undercurl
  hi SpellCap guifg=#f99157 guibg=NONE guisp=NONE gui=undercurl cterm=undercurl
  hi SpellLocal guifg=#f99157 guibg=NONE guisp=NONE gui=undercurl cterm=undercurl
  hi SpellRare guifg=#cc99cc guibg=NONE guisp=NONE gui=undercurl cterm=undercurl
  hi Pmenu guifg=fg guibg=#393939 guisp=NONE gui=NONE cterm=NONE
  hi PmenuSel guifg=fg guibg=#393939 guisp=NONE gui=reverse cterm=reverse
  hi PmenuSbar guifg=#262626 guibg=#262626 guisp=NONE gui=NONE cterm=NONE
  hi PmenuThumb guifg=#cc99cc guibg=#cc99cc guisp=NONE gui=NONE cterm=NONE
  hi DiffAdd guifg=NONE guibg=#073655 guisp=NONE gui=NONE cterm=NONE
  hi DiffChange guifg=NONE guibg=#333233 guisp=NONE gui=NONE cterm=NONE
  hi DiffDelete guifg=#262626 guibg=#730b00 guisp=NONE gui=bold cterm=bold
  hi DiffText guifg=NONE guibg=#730b00 guisp=NONE gui=bold cterm=bold
  hi Cursor guifg=#262626 guibg=#99cc99 guisp=NONE gui=NONE cterm=NONE
  hi! link QuickFixLine Search
  hi! link Delimiter Special
  hi Underlined guifg=#6699cc guibg=NONE guisp=NONE gui=underline cterm=underline
  hi diffAdded guifg=#99cc99 guibg=NONE guisp=NONE gui=NONE cterm=NONE
  hi diffRemoved guifg=#f2777a guibg=NONE guisp=NONE gui=NONE cterm=NONE
  hi gitcommitSummary guifg=fg guibg=NONE guisp=NONE gui=bold cterm=bold
  hi Command guifg=#f2777a guibg=NONE guisp=NONE gui=NONE cterm=NONE
  hi Error guifg=#f2777a guibg=NONE guisp=NONE gui=NONE cterm=NONE
  hi ErrorMsg guifg=#f2777a guibg=NONE guisp=NONE gui=NONE cterm=NONE
  hi Ignore guifg=#393939 guibg=NONE guisp=NONE gui=NONE cterm=NONE
  hi Conceal guifg=#393939 guibg=NONE guisp=NONE gui=NONE cterm=NONE
  if !s:italics
    hi Identifier gui=NONE cterm=NONE
  endif
  if get(g:Vsd, 'extra_highlight', 1)
    hi GitGutterAdd guifg=#99cc99 guibg=NONE guisp=NONE gui=NONE cterm=NONE
    hi GitGutterChange guifg=#f2c38f guibg=NONE guisp=NONE gui=NONE cterm=NONE
    hi GitGutterChangeDelete guifg=#f2c38f guibg=NONE guisp=NONE gui=NONE cterm=NONE
    hi GitGutterDelete guifg=#f2777a guibg=NONE guisp=NONE gui=NONE cterm=NONE
    hi SignifySignAdd guifg=#99cc99 guibg=NONE guisp=NONE gui=NONE cterm=NONE
    hi SignifySignChange guifg=#f2c38f guibg=NONE guisp=NONE gui=NONE cterm=NONE
    hi SignifySignChangeDelete guifg=#f2c38f guibg=NONE guisp=NONE gui=NONE cterm=NONE
    hi SignifySignDelete guifg=#f2777a guibg=NONE guisp=NONE gui=NONE cterm=NONE
    hi! link helpCommand Type
    hi! link helpSectionDelim PreProc
    hi! link HelpHeadline Statement
    hi cSpecial guifg=#f2777a guibg=NONE guisp=NONE gui=NONE cterm=NONE
    hi! link cBraces NonText
  endif
  let g:terminal_ansi_colors = [
        \ "#3C4C55",
        \ "#DF8C8C",
        \ "#A8CE93",
        \ "#DADA93",
        \ "#83AFE5",
        \ "#9A93E1",
        \ "#83AFE5",
        \ "#C5D4DD",
        \ "#608b4e",
        \ "#F2C38F",
        \ "#A8CE93",
        \ "#DADA93",
        \ "#83AFE5",
        \ "#D18EC2",
        \ "#83AFE5",
        \ "#E6EEF3"
        \]

  " NEOVIM TERMINAL MODE
  let g:terminal_color_0 = "#3C4C55"
  let g:terminal_color_1 = "#DF8C8C"
  let g:terminal_color_2 = "#A8CE93"
  let g:terminal_color_3 = "#DADA93"
  let g:terminal_color_4 = "#83AFE5"
  let g:terminal_color_5 = "#9A93E1"
  let g:terminal_color_6 = "#83AFE5"
  let g:terminal_color_7 = "#C5D4DD"
  let g:terminal_color_8 = "#608b4e"
  let g:terminal_color_9 = "#F2C38F"
  let g:terminal_color_10 = "#A8CE93"
  let g:terminal_color_11 = "#DADA93"
  let g:terminal_color_12 = "#83AFE5"
  let g:terminal_color_13 = "#D18EC2"
  let g:terminal_color_14 = "#83AFE5"
  let g:terminal_color_15 = "#E6EEF3"

  " FZF
  let g:fzf_colors = {
        \ "fg":      ["fg", "Normal"],
        \ "none":      ["none", "Normal"],
        \ "hl":      ["fg", "Conditional"],
        \ "fg+":     ["fg", "CursorLine", "CursorColumn", "Normal"],
        \ "none+":     ["none", "CursorLine", "CursorColumn"],
        \ "hl+":     ["fg", "Conditional"],
        \ "info":    ["fg", "Conditional"],
        \ "border":  ["fg", "Ignore"],
        \ "prompt":  ["fg", "Comment"],
        \ "pointer": ["fg", "Conditional"],
        \ "marker":  ["fg", "Conditional"],
        \ "spinner": ["fg", "Conditional"],
        \ "header":  ["fg", "Conditional"]
        \}
  unlet s:t_Co s:italics
  finish
endif

if s:t_Co >= 256
  hi Normal ctermfg=252 ctermbg=235 cterm=NONE
  if !has('patch-8.0.0616') " Fix for Vim bug
    set background=dark
  endif
  call vsd#init('tomorrow_eighties')
  hi EndOfBuffer ctermfg=237 ctermbg=NONE cterm=NONE
  hi LineNr ctermfg=237 ctermbg=NONE cterm=NONE
  hi NonText ctermfg=237 ctermbg=NONE cterm=NONE
  hi Search ctermfg=235 ctermbg=223 cterm=NONE
  hi IncSearch ctermfg=235 ctermbg=209 cterm=bold
  hi TabLine ctermfg=237 ctermbg=fg cterm=reverse
  hi TabLineFill ctermfg=237 ctermbg=fg cterm=reverse
  hi StatusLine ctermfg=237 ctermbg=222 cterm=reverse
  hi StatusLineNC ctermfg=237 ctermbg=fg cterm=reverse
  hi StatusLineTerm ctermfg=237 ctermbg=222 cterm=reverse
  hi StatusLineTermNC ctermfg=237 ctermbg=fg cterm=reverse
  hi VertSplit ctermfg=235 ctermbg=237 cterm=NONE
  hi Visual ctermfg=NONE ctermbg=24 cterm=NONE
  hi VisualNOS ctermfg=NONE ctermbg=24 cterm=NONE
  hi Directory ctermfg=67 ctermbg=NONE cterm=NONE
  hi ModeMsg ctermfg=151 ctermbg=NONE cterm=NONE
  hi MoreMsg ctermfg=151 ctermbg=NONE cterm=NONE
  hi Question ctermfg=151 ctermbg=NONE cterm=NONE
  hi WarningMsg ctermfg=210 ctermbg=NONE cterm=NONE
  hi MatchParen ctermfg=210 ctermbg=237 cterm=bold
  hi Folded ctermfg=247 ctermbg=NONE cterm=NONE
  hi FoldColumn ctermfg=fg ctermbg=NONE cterm=NONE
  hi CursorLine ctermfg=NONE ctermbg=237 cterm=NONE
  hi CursorLineNr ctermfg=222 ctermbg=NONE cterm=NONE
  hi CursorColumn ctermfg=fg ctermbg=237 cterm=NONE
  hi SignColumn ctermfg=fg ctermbg=NONE cterm=NONE
  hi ColorColumn ctermfg=fg ctermbg=237 cterm=NONE
  hi Comment ctermfg=247 ctermbg=NONE cterm=NONE
  hi Todo ctermfg=247 ctermbg=NONE cterm=NONE
  hi Title ctermfg=247 ctermbg=NONE cterm=NONE
  hi Constant ctermfg=209 ctermbg=NONE cterm=NONE
  hi String ctermfg=151 ctermbg=NONE cterm=NONE
  hi Identifier ctermfg=fg ctermbg=NONE cterm=italic
  hi Function ctermfg=39 ctermbg=NONE cterm=NONE
  hi Statement ctermfg=210 ctermbg=NONE cterm=NONE
  hi Conditional ctermfg=67 ctermbg=NONE cterm=NONE
  hi Repeat ctermfg=67 ctermbg=NONE cterm=NONE
  hi Label ctermfg=67 ctermbg=NONE cterm=NONE
  hi Operator ctermfg=80 ctermbg=NONE cterm=NONE
  hi Type ctermfg=222 ctermbg=NONE cterm=NONE
  hi Structure ctermfg=222 ctermbg=NONE cterm=NONE
  hi Special ctermfg=223 ctermbg=NONE cterm=NONE
  hi SpecialKey ctermfg=223 ctermbg=NONE cterm=NONE
  hi PreProc ctermfg=182 ctermbg=NONE cterm=NONE
  hi Define ctermfg=182 ctermbg=NONE cterm=NONE
  hi Include ctermfg=182 ctermbg=NONE cterm=NONE
  hi SpellBad ctermfg=210 ctermbg=NONE cterm=undercurl
  hi SpellCap ctermfg=209 ctermbg=NONE cterm=undercurl
  hi SpellLocal ctermfg=209 ctermbg=NONE cterm=undercurl
  hi SpellRare ctermfg=182 ctermbg=NONE cterm=undercurl
  hi Pmenu ctermfg=fg ctermbg=237 cterm=NONE
  hi PmenuSel ctermfg=fg ctermbg=237 cterm=reverse
  hi PmenuSbar ctermfg=235 ctermbg=235 cterm=NONE
  hi PmenuThumb ctermfg=182 ctermbg=182 cterm=NONE
  hi DiffAdd ctermfg=NONE ctermbg=24 cterm=NONE
  hi DiffChange ctermfg=NONE ctermbg=236 cterm=NONE
  hi DiffDelete ctermfg=235 ctermbg=88 cterm=bold
  hi DiffText ctermfg=NONE ctermbg=88 cterm=bold
  hi Cursor ctermfg=235 ctermbg=151 cterm=NONE
  hi! link QuickFixLine Search
  hi! link Delimiter Special
  hi Underlined ctermfg=67 ctermbg=NONE cterm=underline
  hi diffAdded ctermfg=151 ctermbg=NONE cterm=NONE
  hi diffRemoved ctermfg=210 ctermbg=NONE cterm=NONE
  hi gitcommitSummary ctermfg=fg ctermbg=NONE cterm=bold
  hi Command ctermfg=210 ctermbg=NONE cterm=NONE
  hi Error ctermfg=210 ctermbg=NONE cterm=NONE
  hi ErrorMsg ctermfg=210 ctermbg=NONE cterm=NONE
  hi Ignore ctermfg=237 ctermbg=NONE cterm=NONE
  hi Conceal ctermfg=237 ctermbg=NONE cterm=NONE
  if !s:italics
    hi Identifier cterm=NONE
  endif
  if get(g:Vsd, 'extra_highlight', 1)
    hi GitGutterAdd ctermfg=151 ctermbg=NONE cterm=NONE
    hi GitGutterChange ctermfg=223 ctermbg=NONE cterm=NONE
    hi GitGutterChangeDelete ctermfg=223 ctermbg=NONE cterm=NONE
    hi GitGutterDelete ctermfg=210 ctermbg=NONE cterm=NONE
    hi SignifySignAdd ctermfg=151 ctermbg=NONE cterm=NONE
    hi SignifySignChange ctermfg=223 ctermbg=NONE cterm=NONE
    hi SignifySignChangeDelete ctermfg=223 ctermbg=NONE cterm=NONE
    hi SignifySignDelete ctermfg=210 ctermbg=NONE cterm=NONE
    hi! link helpCommand Type
    hi! link helpSectionDelim PreProc
    hi! link HelpHeadline Statement
    hi cSpecial ctermfg=210 ctermbg=NONE cterm=NONE
    hi! link cBraces NonText
  endif
  let g:terminal_ansi_colors = [
        \ "#3C4C55",
        \ "#DF8C8C",
        \ "#A8CE93",
        \ "#DADA93",
        \ "#83AFE5",
        \ "#9A93E1",
        \ "#83AFE5",
        \ "#C5D4DD",
        \ "#608b4e",
        \ "#F2C38F",
        \ "#A8CE93",
        \ "#DADA93",
        \ "#83AFE5",
        \ "#D18EC2",
        \ "#83AFE5",
        \ "#E6EEF3"
        \]

  " NEOVIM TERMINAL MODE
  let g:terminal_color_0 = "#3C4C55"
  let g:terminal_color_1 = "#DF8C8C"
  let g:terminal_color_2 = "#A8CE93"
  let g:terminal_color_3 = "#DADA93"
  let g:terminal_color_4 = "#83AFE5"
  let g:terminal_color_5 = "#9A93E1"
  let g:terminal_color_6 = "#83AFE5"
  let g:terminal_color_7 = "#C5D4DD"
  let g:terminal_color_8 = "#608b4e"
  let g:terminal_color_9 = "#F2C38F"
  let g:terminal_color_10 = "#A8CE93"
  let g:terminal_color_11 = "#DADA93"
  let g:terminal_color_12 = "#83AFE5"
  let g:terminal_color_13 = "#D18EC2"
  let g:terminal_color_14 = "#83AFE5"
  let g:terminal_color_15 = "#E6EEF3"

  " FZF
  let g:fzf_colors = {
        \ "fg":      ["fg", "Normal"],
        \ "none":      ["none", "Normal"],
        \ "hl":      ["fg", "Conditional"],
        \ "fg+":     ["fg", "CursorLine", "CursorColumn", "Normal"],
        \ "none+":     ["none", "CursorLine", "CursorColumn"],
        \ "hl+":     ["fg", "Conditional"],
        \ "info":    ["fg", "Conditional"],
        \ "border":  ["fg", "Ignore"],
        \ "prompt":  ["fg", "Comment"],
        \ "pointer": ["fg", "Conditional"],
        \ "marker":  ["fg", "Conditional"],
        \ "spinner": ["fg", "Conditional"],
        \ "header":  ["fg", "Conditional"]
        \}
  unlet s:t_Co s:italics
  finish
endif

" Background: dark
" Color:	    white	#cccccc ~
" Color:	    black	#262626 ~
" Color:	    selection	#264f78 ~
" Color:	    comment	#999999 ~
" Color:	    red		#f2777a ~
" Color:	    orange	#f99157 ~
" Color:	    yellow	#ffcc66 ~
" Color:	    green	#99cc99 ~
" Color:	    aqua	#66cccc ~
" Color:	    blue	#6699cc ~
" Color:	    brightblue	#61afef ~
" Color:	    purple	#cc99cc ~
" Color:	    window	#393939 ~
" Color:	    special	#f2c38f ~
" Color:	    lightgrey	#a9a9a9 ~
" Color:	    darkblue	#073655 ~
" Color:	    darkred	#730b00 ~
" Color:	    darkgrey	#333233 ~
" vim: et ts=2 sw=2
